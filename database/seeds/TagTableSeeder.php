<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = collect([
            'pendidikan', 
            'edukasi',
            'pelajaran ',
            'bahasa indonesia ',
            'pancasila',
            'vaksin corona',
            'pandemi corona',
            'indonesia',
            'virus corona',
            'corona',
            'vaksin covid-19',
            'update corona',
            'banjir',
            'kecelakaan',
            'lansia',
            'shop',
            'kebakaran',
            'narkoba',
            'penjara',
            'kendaraan',
        ]);

        $tags->each( function($tag) {

            Tag::create([
                'nama' => $tag
            ]);
        });
    }
}
