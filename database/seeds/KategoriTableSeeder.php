<?php

use Illuminate\Database\Seeder;
use App\Kategori;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategoris = collect([
            'Nasional',
            'Internasional',
            'Ekonomi',
            'Teknologi',
            'Kesehatan',
            'Olahraga',
            'Otomotif',
            'Politik',
            'Game',
            'Bencana',
            'Kriminal',
        ]);

        $kategoris->each( function($kategori) {

            Kategori::create([
                'nama' => $kategori
            ]);
        });
    }
}
