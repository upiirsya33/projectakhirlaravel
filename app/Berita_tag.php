<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita_tag extends Model
{
    protected $table = "berita_tag";
    protected $fillable = ['berita_id','tag_id'];
    
    // Simpan sebagai catatan buat kedepan !
    // Model tidak berfungsi dan tidak berelasi ?, sebaiknya dihilangkan ?

    // public function berita(){
    //         return $this->belongsToMany('App\Berita');
    //     }

    // public function tag(){
    //         return $this->belongsToMany('App\Tag');
    //     }
}
