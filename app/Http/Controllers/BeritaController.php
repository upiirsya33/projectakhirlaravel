<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Tag;
use App\Berita;
use App\User;
use App\Kategori;
use App\Berita_tag;

use DB;
use File;

class BeritaController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::paginate(5);
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $berita = Berita::with('user_id')->get();
        $tag = Tag::all();
        $kategori = DB::table('kategori')->get();
        return view('berita.create', compact('kategori', 'tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'gambar' => 'mimes:jpeg,jpg,png|max:2200',
    		'kategori_id' => 'required',
            'tag' => 'required',
            'teks' => 'required'
    	]);

        $gambar = $request->gambar;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $g = Berita::create([
    		'judul' => $request->judul,
    		'gambar' => $new_gambar,
            'kategori_id' => $request->kategori_id,
            'teks' => $request->teks,
            'user_id' => auth()->id()
    	]);
        
        $berita = Berita::find($g->id); //ambil id di tabel berita yang baru di create
                                        // harus pake model gak bisa aeperti $berita = $g->id;

        //// =======================================================
        // latihan pengecekan input masuk ke tabel berita_tag
        // $berita = $g->id;
        // dd($berita);
        // $berita1 = 1;
        // $ss = [10, 11];
        //berita1->tag()->attach($ss); //tidak bisa karena $berita1 tidak ambil data dari model
        // =======================================================

        //passing data ke dalam tabel berita_tag isinya berita_id
        $berita->tag()->attach($request->tag);
       

        $gambar->move('gambar/', $new_gambar);
 
    	return redirect('/berita')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);   
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);
        $kategori = DB::table('kategori')->get();
        $tag = DB::table('tag')->get();
        $berita_tag = Berita_tag::where('berita_id', $id)->get();
        // $berita_tag = Berita_tag::where('berita_id', $id)->pluck('tag_id')->toArray();
        // dd($berita_tag);

        return view('berita.edit', compact('berita', 'kategori', 'tag', 'berita_tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'judul' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
    		'kategori_id' => 'required',
            'tag' => 'required',
            'teks' => 'required'

        ]);

        $berita = Berita::findorfail($id);

        if ($request->has('gambar')){
            $path = "gambar/";
            File::delete($path . $berita->gambar);
            $gambar = $request->gambar;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move('gambar/', $new_gambar)    ;
            $berita_data = [
                'judul' => $request->judul,
                'gambar' => $new_gambar,
                'kategori_id' => $request->kategori_id,
                'teks' => $request->teks
            ];
        }else{
            $berita_data = [
                'judul' => $request->judul,
                'kategori_id' => $request->kategori_id,
                'teks' => $request->teks
            ];
        }
       
        // $attributes = ['tag_id', $request->tag];
        // $berita->tag()->updateExistingPivot($id, $attributes);

        $berita->tag()->sync($request->tag);
        $berita->update($berita_data);
        return redirect('/berita')->with('success', 'Data Berhasil Diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // =======================================================
        // $trophyIds  = Berita_tag::where('berita_id',$id)
        //             ->pluck('id')->toArray(); 
        // dd($trophyIds);
        //  =======================================================
        $berita = Berita::find($id);
        $berita->tag()->detach(); //hapus tabel berita_tag dulu
        $berita->delete(); // baru hapus tabel berita
        $path = "gambar/";
        File::delete($path . $berita->gambar);
        return redirect('/berita')->with('success', 'Data Berhasil Dihapus!');
    }
}
