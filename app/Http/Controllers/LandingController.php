<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Kategori;
use DB;


class LandingController extends Controller
{
    public function index(Request $request){
        $berita = Berita::all();
        // dd($berita);
        return view('landing.index', compact('berita'));
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari model berita sesuai pencarian data
		$berita = Berita::where('judul','like',"%".$cari."%")->get();
 
    		// mengirim data berita ke view index
        return view('landing.index', compact('berita'));
 
	}

    public function berita_kategori($nama){
       // menangkap data pencarian
		$kategori = $nama;
        
        $kategori1 = DB::table('kategori')->select('id')->where('nama','like',"%".$kategori."%")->first();
        // $kategori1 = Kategori::select('id')->where('nama','like',"%".$kategori."%")->get();

        // dd($kategori1->id);
      
        $berita = Berita::where('kategori_id', $kategori1->id)->get();
        // dd($berita);
        // mengirim data berita ke view index
       return view('landing.index', compact('berita'));
    }

}
