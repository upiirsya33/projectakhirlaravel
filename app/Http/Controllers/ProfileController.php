<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        // $user = User::find($id);
        // $profile = Profile::where('user_id', $id)->first();
        // return view('profile.index', compact(['user', 'profile']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            // 'name' => 'required',
            // 'email' => 'required',
    		'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required'
        ]);
            $profil_data =[
                'umur' => $request->umur,
                'alamat' => $request->alamat,
                'bio' => $request->bio,
        ];

        // $user_data =[
        //     'name' => $request->name,
        //     'email' => $request->email,
        // ];
        // User::whereId($id)->update($user_data);

        // $id dari index profile nilainya {{$profile->id}} = id profil, 
        // Profile::whereId($id)->update($profil_data); sama saja
        // Profile::where('id',$id)->update($profil_data); sama saja

        //$id dari index profile nilainya {{$profile->user_id}} id user di tabel profile
        Profile::where('user_id',$id)->update($profil_data);

        return redirect('/profile')->with('success', 'Data Berhasil Diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
