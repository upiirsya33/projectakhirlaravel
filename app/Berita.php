<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = "berita";
    protected $fillable = ["judul","gambar","teks","kategori_id","user_id"];

    public function tag(){
        return $this->belongsToMany('App\Tag');
        // return $this->belongsToMany('App\Tag')->withPivot('id','berita_id', 'tag_id');
    }

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    // public function tags(){
    //     return $this->hasMany('App\Berita_Tag');
       
    // }

}
