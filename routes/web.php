<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "LandingController@index");
Route::get('/cari', "LandingController@cari");
Route::get('/berita_kategori/{nama}', "LandingController@berita_kategori");

//Route::get('/Berita-kategori/{kategori}', "Web\LandingController@berita_kategori")->name('berita.kategori');


Route::get('/dashboard', 'DashboardController@index');


// CRUD Kategori
Route::resource('/kategori', 'KategoriController');

// CRUD tag
Route::resource('tag', 'TagController');


// CRUD Berita
Route::resource('berita', 'BeritaController');


Route::resource('profile', 'ProfileController');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//DomPDF
//Route::get('/test-dompdf', function(){
    //$pdf = App::make('dompdf.wrapper');
    //$pdf->loadHTML('<h1>Test</h1>');
    //return $pdf->stream();
//});

//Route::get('/test-dompdf-2', 'PdfController@test');


