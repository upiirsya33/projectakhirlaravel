@extends('layouts.masterlanding')



@section('isi')

  <section class="text-center my-3">
    <h2 class = "text-primary">Berita Terbaru</h2>
  </section>

  <div class="container">
      @forelse ($berita as $key=>$value)
      <ul class="list-unstyled">
          <li class="media my-5 border">
              <img class="mr-2" src="{{asset('gambar/'. $value->gambar)}}" alt="Generic placeholder image" width="200" height="200">
              <div class="media-body">
                  <h5 class="mt-0 mb-1">{{$value->judul}}</h5>
                  <p>{!!Str::limit($value->teks)!!}</p>
                  <a class="btn btn-primary" data-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">Baca Selengkapnya</a>
              </div>
          </li>
          {{-- cek data dalam tabel kosong --}}
      @empty
          <tr>
          <td  colspan="5" style="text-align:center" >
              <h4 class="text-center">
              Tidak ada data dalam database
              </h4>
          </td>
          </tr>  
      @endforelse
      
      </ul>
  </div>
  @endsection