@extends('layouts.master')

@section('judul')
        Detail Berita {{$berita->id}}
@endsection

@section('isi')

        <div class="mx-3 my-3">
            <div class="card card-primary">
                <div class="mx-3 my-3">
                    <center>
                    <h1 style="color:black" >{{$berita->judul}}</h1>
                    <ul>
                    <i class="fa fa-folder" style="color:blue">&ensp;{{$berita->kategori->nama}}</i>
                    <i class="fa fa-tags" style="color:purple">
                        @foreach ($berita->tag as $tag)
                        {{$tag->nama}}
                        @endforeach
                    </i>
                    </ul>

                    <img src="{{asset('gambar/'.$berita->gambar)}}" alt="..." width="600" height="400">
                    </center>
                    <p>{!!$berita->teks!!}</p>

                    <div class="d-flex justify-content-end">
                        <a href="/berita" class="btn btn-info">Back</a>
                    </div>
                    
                </div>
            </div>
        </div>

@endsection