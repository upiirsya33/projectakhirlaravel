@extends('layouts.master')

@section('judul')
      Data Berita      
@endsection

@section('isi')

    <div class="mx-3 my-1">
            
        @if(session('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span>  <b>{{ session('success')}}</b>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

            <div class="mb-3">
                <a href="/berita/create" class="btn btn-primary">Tambah Berita</a>
            </div>

        <div class="card card-primary">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align:center">No</th>
                    <th scope="col" style="text-align:center">Judul</th>
                    <th scope="col" style="text-align:center">Gambar</th>
                    <th scope="col" style="text-align:center">Kategori</th>
                    <th scope="col" style="text-align:center">Tag</th>
                    <th scope="col" style="text-align:center">Penulis</th>
                    <th scope="col" style="text-align:center">Tanggal Posting</th>
                    <th scope="col" style="text-align:center">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($berita as $key=>$value)
                        <tr>
                            <td style="text-align:center">{{$key + $berita->firstitem()}}</th>
                            <td style="text-align:center">{{$value->judul}}</td>
                            <td style="text-align:center"><img src="{{asset('gambar/'. $value->gambar)}}" alt="" height="60" width="60"></td>
                            <td style="text-align:center">{{$value->kategori->nama}}</td>
                            <td style="text-align:center">
                                @foreach ($value->tag as $tag)

                                    {{$tag->nama}}

                                 @endforeach
                            </td>
                            <td style="text-align:center">{{$value->user->name}}</td>
                            <td style="text-align:center">{{$value->created_at}}</td>
                            <td class="d-flex justify-content-center">
                                    <a href="/berita/{{$value->id}}" class="btn btn-info btn-sm my-3 mx-1">Show</a>

                                @if(Auth::user()->id === $value->user->id)
                                    <a href="/berita/{{$value->id}}/edit" class="btn btn-primary btn-sm my-3 mx-1">Edit</a>
                                    <form action="/berita/{{$value->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger btn-sm my-3 mx-1" value="Delete">
                                    </form>
                                
                                @endif
                            </td>
                        </tr>
                    {{-- cek data dalam tabel kosong --}}
                    @empty
                        <tr>
                            <td  colspan="5" style="text-align:center" >Tidak Ada Data Di Database</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {{$berita->links()}}
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="https://cdn.tiny.cloud/1/m9tt1jpeytg90rloos6m41q99vbzfze88b9k8wre7aix9ilb/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
       });
      </script>
    @endpush

@endsection
