@extends('layouts.master')

@section('judul')
            Tambah Data Berita 
@endsection

@section('isi')

<div class="ml-3 mb-3 mt-3 mr-3">
    <div class="card card-primary">
        
        <div class="ml-3 mb-3 mt-3 mr-3">
        <form action="/berita" method="POST" enctype="multipart/form-data">
            @csrf

                <div class="form-group">
                    <label for="judul">Judul Berita</label>
                    <input type="text" class="form-control mb-3" name="judul" id="judul"  placeholder="Masukkan Judul Berita">
                        @error('judul')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                        @enderror
                </div>
                
                <div class="form-group">
                    <label for="gambar">Gambar Berita</label>
                    <input type="file" class="form-control mb-3" name="gambar" id="gambar">
                        @error('gambar')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="kategori_id">Kategori</label>
                    <select name="kategori_id" class="form-control mb-3" id="kategori_id">
                        <option value="" disabled> --Pilih Kategori--</option>
                        @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>        
                    @error('kategori_id')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                    </div>

                    <div class="form-group">
                        <label for="tag_id">Tag</label>
                        <select id="tag_id" name="tag[]" class="form-control js-example-placeholder-multiple" multiple="multiple">
                            @foreach ($tag as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select> 
                        @error('tag')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                <div>
                    <label for="teks">Isi Teks Berita</label>
                    <textarea class="form-control" rows ="5" name="teks" id="teks" placeholder="Masukkan Isi Berita"></textarea>
                        @error('teks')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                        @enderror
                </div>
                
        
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
</div>
</div>
@endsection


@push('scripts')

        {{-- library cek editor--}}

        <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

        <script>
            CKEDITOR.replace('teks');
        </script>

        {{-- library select2--}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        
        {{-- jangan digunakan, nanti bentrok ke sidebar --}}
        {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> --}}

        <script type="text/javascript">
            $('.js-example-placeholder-multiple').select2({
                placeholder: "Pilih Tag Berita"
            });
        </script>

@endpush


