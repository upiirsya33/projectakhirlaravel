@extends('layouts.master')

@section('judul')
      Profil      
@endsection

@section('isi')

{{-- <div class="col-md-4 col-xs-12">
        <div class="card">
            <div class="card-body">
                <center class="mt-4"> <img id="user-image" class="rounded-circle" width="150" src="https://sanbercode.com/images/dummy_user.png">
                    <h4 class="card-title mt-2" id="nama-lengkap">{{Auth::user()->name}}</h4>
                    <h6 class="card-subtitle">{{Auth::user()->email}}</h6>
                </center>
            </div>
            <div class="user-btm-box">
                <p>Ganti gambar profil</p>
                <form id="formUploadPhoto">
                    <input type="file" id="userPhoto" name="picture" class="form-control" required="">
                    <br>
                    <button type="submit" id="btnUploadPhoto" class="btn btn-primary btn-small">Submit</button>
                    <img id="loader-gif" src="https://sanbercode.com/images/loader.gif" width="16px" alt="" style="display:none">
                </form>
            </div>
        </div>
</div> --}}

        @if(session('success'))
                <div class="col-sm-12">
                    <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span>  <b>{{ session('success')}}</b>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            
<div class="col-md-8 col-xs-12">
        <div class="card">
                <div class="tab-pane data-diri active show" id="home">
                    <form action="/profile/{{$profile->user_id}}" method="POST" class="form-horizontal form-material" id="data-diri">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label class="col-md-12">Nama</label>
                            <div class="col-md-12">
                                <input type="text" name="name" value="{{Auth::user()->name}}"class="form-control form-control-line" placeholder="Masukkan nama" disabled> 
                                                                    {{-- $profile->user()->name --}}
                            </div>
                        </div>
                        <div class="form-group clearfix">
                        <label class="col-md-12">Email</label>
                            <div class="col-md-12">
                                <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control form-control-line" placeholder="Masukkan email" disabled> 
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-md-12">Umur</label>
                            <div class="col-md-12">
                                <input type="text" name="umur" value="{{$profile->umur}}" class="form-control form-control-line" placeholder="Masukkan umur"> 
                            </div>

                            @error('umur')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Alamat</label>
                            <div class="col-md-12">
                                <input type="text" name="alamat" value="{{$profile->alamat}}" class="form-control form-control-line" placeholder="Masukkan alamat"> 
                            </div>

                            @error('alamat')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label class="col-md-12"for="teks">Biodata</label>
                            <div class="col-md-12"> 
                            <textarea  rows ="5" name="bio" class="form-control form-control-line">{{$profile->bio}}</textarea>
                            </div> 

                            @error('bio')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror     
                        </div>
                       
                        <div class="form-group">
                        <div class="col-sm-12"><br>
                            <button class="btn btn-success" id="btn-data-diri">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection