@extends('layouts.master')
@section('judul')
	Edit Data Kategori {{$kategori->id}}
@endsection
@section('isi')
<div class="ml-3 mb-3 mt-3 mr-3">
    <div class="card card-primary">
        
        <div class="ml-3 mb-3 mt-3 mr-3">
<form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}" id="nama" placeholder="Masukkan Kategori">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>
</div>
@endsection