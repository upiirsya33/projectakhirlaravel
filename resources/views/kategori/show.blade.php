@extends('layouts.master')
@section('judul')
	Detail Kategori {{$kategori->id}}
@endsection
@section('isi')
<div class="mx-3 my-3">
	<div class="card card-primary">
		<div class="mx-3 my-3">
			
			<h1 style="color:magenta" >{{$kategori->nama}}</h1>
			

			<div class="d-flex justify-content-end">
				<a href="/kategori" class="btn btn-info">Back</a>
			</div>
			
		</div>
	</div>
</div>
@endsection