@extends('layouts.master')
@section('judul')
        Tambah Data Kategori
@endsection
@section('isi')
<div class="ml-3 mb-3 mt-3 mr-3">
    <div class="card card-primary">
        
        <div class="ml-3 mb-3 mt-3 mr-3">
<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Kategori</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Kategori">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
        </div>
    </div>
</div>
@endsection