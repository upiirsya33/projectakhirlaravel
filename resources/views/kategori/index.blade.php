@extends('layouts.master')
@section('judul')
	Data Kategori
@endsection
@section('isi')

        <div class="mx-3 my-1">

                @if(session('success'))
                <div class="col-sm-12">
                    <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span>  <b>{{ session('success')}}</b>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                <div class="mb-3">
                    <a href="/kategori/create" class="btn btn-primary">Tambah Kategori</a>
                </div>
        
            <div class="card card-primary">
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col" style="text-align:center">No</th>
                        <th scope="col" style="text-align:center">Nama Kategori</th>
                        <th scope="col" style="text-align:center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($kategori as $key=>$value)
                            <tr>
                                <td style="text-align:center">{{$key + $kategori->firstitem()}}</td>
                                <td style="text-align:center">{{$value->nama}}</td>
                                <td class="d-flex justify-content-center">
                                    <a href="/kategori/{{$value->id}}" class="btn btn-info btn-sm my-1 mx-1">Show</a>
                                    <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary btn-sm my-1 mx-1">Edit</a>
                                    <form action="/kategori/{{$value->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger btn-sm my-1 mx-1" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td  colspan="3" style="text-align:center" >Tidak Ada Data Di Database</td>
                            </tr>  
                        @endforelse              
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
               {{$kategori->links()}}
                </div>
            </div>
        </div>
@endsection