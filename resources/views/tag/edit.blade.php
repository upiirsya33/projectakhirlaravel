@extends('layouts.master')

@section('judul')
            Edit Data Tag {{$tag->id}}
@endsection

@section('isi')

<div class="ml-3 mb-3 mt-3 mr-3">
    <div class="card card-primary">
        
        <div class="ml-3 mb-3 mt-3 mr-3">
        <form action="/tag/{{ $tag->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama Tag</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', $tag->nama) }}" placeholder="Masukkan Nama Tag">
                @error('nama')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
    </div>
</div>

@endsection