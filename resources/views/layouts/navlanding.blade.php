<nav class="navbar navbar-expand-lg navbar-light bg-white d-flex justify-content-between border">
    <a class="navbar-brand" href="/">Portal Berita 4</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <form class="form-inline" action="/cari" method="GET">
        <input class="form-control mr-sm-2" type="search" name="cari" placeholder="Cari Berita" aria-label="search" value="{{ old('cari') }}">
        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Cari</button>
    </form>

    <div>
     <a href="{{ route('login') }}">
       <button type="button" class="btn btn-outline-primary btn-white mx-3">{{ __('Login') }}</button>
     </a> 
     <a href="{{ route('register') }}">
       <button type="button" class="btn btn-outline-primary btn-white">{{ __('Register') }}</button>
     </a>
    </div>
    </nav>    


<!-- Navigation -->

<nav class="navbar navbar-expand-lg navbar-dark bg-primary container my-3">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse d-flex justify-content-center" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/berita_kategori/Internasional">Internasional</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/berita_kategori/Nasional">Nasional</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/berita_kategori/Politik">Politik</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/berita_kategori/Olahraga">Olahraga</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/berita_kategori/Kesehatan">Kesehatan</a>
      </li>
    </ul>
  </div>
</nav>