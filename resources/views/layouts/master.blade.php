<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('judul') Portal Berita </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    
    
    
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{ asset('/sufee-admin/assets/scss/style.css') }}">
    {{-- <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet"> --}}

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    @stack('style')

    {{-- Icon Title Pada HTML --}}
    <link rel="icon" href="/logo/RLF.png" alt="Logo">

</head>
<body>


        <!-- Left Panel -->

        @include('layouts.sidebar')  <!-- /#left-panel -->
    
        <!-- Left Panel -->

        <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->

        @include('layouts.navbar') <!-- /header -->
        
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>@yield('judul')</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            
            @yield("isi") 
            
        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{ asset('/sufee-admin/assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{ asset('/sufee-admin/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('/sufee-admin/assets/js/main.js') }}"></script>



    <script src="{{ asset('/sufee-admin/assets/js/lib/chart-js/Chart.bundle.js') }}"></script>
    <script src="{{ asset('/sufee-admin/assets/js/dashboard.js') }}"></script>
    <script src="{{ asset('/sufee-admin/assets/js/widgets.js') }}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}

    @stack('scripts')
    
</body>
</html>
