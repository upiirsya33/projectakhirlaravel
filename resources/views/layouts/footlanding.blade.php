<footer class="bg-primary">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="row-sm my-5 text-white">
                <h3 class = "my-3 font-weight-bold">Portal Berita 4</h3>
                <p class = "my-1 font-italic">"Akses informasi terkini dimana saja kapan saja"</p>

            </div>
            <div class="row-sm my-5">
                <ul style="list-style-type: none;">
                    <li>
                        <a class ="text-white" href="/berita_kategori/Internasional">Internasional</a>
                    </li>
                    <li>
                        <a class ="text-white" href="/berita_kategori/Nasional">Nasional</a>
                    </li>
                    <li>
                        <a class ="text-white" href="/berita_kategori/Politik">Politik</a>
                    </li>
                    <li>
                        <a class ="text-white" href="/berita_kategori/Olahraga">Olahraga</a>
                    </li>
                    <li>
                        <a class ="text-white" href="/berita_kategori/Kesehatan">Kesehatan</a>
                    </li>
                </ul>
            </div>
            <div class="row-sm mt-5 text-white">
                <h3 class = "my-3">Contact Us</h3>
                <p class = "my-1">Email : portalberita4@gmail.com</p>
                <p class = "my-1">Telpon : 081200050006</p>
            </div>
        </div>
    </div>
    <p class = "text-center text-white mb-0">@Copyright PortalBerita4@2021</p>
</footer>